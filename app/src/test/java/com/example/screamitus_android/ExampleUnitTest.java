package com.example.screamitus_android;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    Infection infection;
//    @Test
//    public void addition_isCorrect() {
//        assertEquals(4, 2 + 2);
//    }

    @Before
    public void setUp() throws Exception {
        infection = new Infection();
    }


    //R1: number of days must be greater than 0
    @Test
    public void testNumberOfInfectedInstructorsWhen0day(){

        //R1: number of days must be greater than 0
        int n = 0;
        assertEquals(-1, infection.calculateTotalInfected(n));

    }

    //R2: virus infects at a rate of 5 till 7 days
    @Test
    public  void testNumberOfInfectedTill7Days(){
        int n = 2;
        assertEquals(10,infection.calculateTotalInfected(n));
    }

    //R3: virus infects at a rate of 8 after 7 days
    @Test
    public  void testNumberOfInfectedAfter7Days(){
        int n = 8;
        assertEquals(43,infection.calculateTotalInfected(n));
    }

}