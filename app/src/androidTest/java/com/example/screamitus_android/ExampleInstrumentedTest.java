package com.example.screamitus_android;
import android.support.test.rule.ActivityTestRule;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.screamitus_android", appContext.getPackageName());
    }

    // Put the name of the screen you want android to start on
    @Rule
    public ActivityTestRule activityRule =
            new ActivityTestRule<>(MainActivity.class);

    //TC1: on app load, textbox and button are visible, results label is hidden
    @Test
    public void  testOnloadApp() {
        onView(withId(R.id.daysTextBox)).check(matches(isDisplayed()));
        onView(withId(R.id.btnCalculate)).check(matches(isDisplayed()));
        onView(withId(R.id.resultsLabel)).check(matches(isDisplayed()));

    }

    // TC2: When you enter a value in the textbox and press calculate
    //the app displays the correct number of infected instructors
    @Test
    public void testInputBox() throws InterruptedException {
        // 1. Find the text box
        // 2. Type something into the text box
        onView(withId(R.id.daysTextBox)).perform(typeText("2"));
        Thread.sleep(2000);

        //  3. push the button
        onView(withId(R.id.btnCalculate))
                .perform(click());

        Thread.sleep(2000);

        // 4. Check if the output is correct
        String expectedOutput = "10";
        onView(withId(R.id.resultsLabel))
                .check(matches(withText(expectedOutput)));

        Thread.sleep(2000);

    }

}
