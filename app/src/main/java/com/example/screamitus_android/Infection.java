package com.example.screamitus_android;

public class Infection {

    public Infection() {
        //empty constructor
    }

    public int calculateTotalInfected(int day) {
        int totalNumberOfInfectedInstructors = 0;

        //if day is from 1-7
        if (day > 0 && day < 8) {
            totalNumberOfInfectedInstructors = day * 5;
            return totalNumberOfInfectedInstructors;
        }

        //if day > 7
        else if (day > 7 ) {
            int numberOfInfectedInstructorsTillDay7 = 35;
            totalNumberOfInfectedInstructors = numberOfInfectedInstructorsTillDay7 + ((day - 7) * 8);
            return  totalNumberOfInfectedInstructors;
        }

        //if day is <= 0
        else return -1;
    }

}
